Quick Site
==========

Quick Site is a simple, easy way to make your own website.


Instructions
------------

1. Install [Lektor](https://www.getlektor.com/)
2. Fork this repository and clone it locally.
3. Change the details in `quicksite.lektorproject`.
4. See how content is laid out under `content/` and add your own.
5. Run `lektor serve` and visit http://localhost:5000/ to see your site.
6. Commit and push to see it live on https://yourusername.gitlab.io/quicksite/


Miscellaneous
-------------

To enable [Neocities](https://neocities.org/) deployment, you need to go to your Neocities site settings, and generate
a new API key. Copy the key, then go to your Gitlab CI settings at:

https://gitlab.com/<yourusername>/quicksite/-/settings/ci_cd

and add a new variable called `NEOCITIES_TOKEN`. The value should be your Neocities API key, "Type" should be "File",
and "Environment scope" should be "neocities", if that's in there. If not, no biggie. Check "protected" and "masked",
and [you're done](https://quicksite.neocities.org).


Demo
----

You can see a demo of the contents of this repository here:

https://quicksite.stavros.io/
