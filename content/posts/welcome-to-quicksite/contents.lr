title: Welcome to Quick Site!
---
pub_date: 2020-06-01
---
body:

![](photo.jpg)

> Sometimes, quotes are just there to look nice.
>
> – Winston Churchill

Welcome to Quick Site! Quick Site is an open source project that aims to help you create your own, good-looking website
with minimum hassle.

Quick Site was born out of my frustrations at not having a good answer when my friends asked me what a good place to
start blogging was. Medium is a walled garden, WordPress is too complicated, Facebook is the second horseman of the
apocalypse, and running your own site is too much of a hassle.

That is, until now. Quick Site is a dead-simple static website that is generated with
[Lektor](https://www.getlektor.com/) and editable from Lektor's own easy-to-use web interface.  Inspired by the old web,
Quick Site uses [NewCSS](https://newcss.net/) to look good while still being really quick to load, and deploys to GitLab
Pages, with optional [Neocities](https://neocities.org/) and [IPFS](https://ipfs.io/) deployments.

Quick Site uses Gitlab CI to build itself, so you can just commit to your repository, push and see your site deployed
automatically, with no other steps for you to take.

Convinced? Just [fork the repository](https://gitlab.com/stavros/quicksite) and get your own site in five minutes.

Not convinced? [Create an issue](https://gitlab.com/stavros/quicksite/-/issues) to tell me what's missing.

On the fence? [Do it anyway](https://gitlab.com/stavros/quicksite).

I hope you like this! If you have any comments, you can find me on [Twitter](https://twitter.com/stavros) or
[Mastodon](https://mastodon.host/@stavros).
